import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

/*
  Generated class for the B2BService provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class B2BService {
  data: any;
  capListData:any
  _platform:any

  constructor(private http: Http) {
    this.data = null;
    this.capListData = null;
  }

  load() {
    if (this.data) {
      // already loaded data
      return Promise.resolve(this.data);
    }
    return new Promise((resolve, reject) => {
      //http://private-c58bd9-naveen4nkp.apiary-mock.com/getdashboard
      this.http.get('mock-json/data.json')
        .map(res => res.json())
        .subscribe(data => {
          this.data = data;
          resolve(this.data);
        },err => {
          reject(err);
        });
    });
  }

  loadCapList(){
    if(this.capListData){
      return Promise.resolve(this.capListData);
    }
    return new Promise((resolve,reject)=>{
      this.http.get('mock-json/cap-list.json')
      .map(res => res.json())
      .subscribe(data => {
        this.capListData = data;
        resolve(this.capListData);
      },err => {
        reject(err);
      })
    })
  }

  setSelectedPlatform(platform){
    this._platform = platform;
  }

  getSelectedPlatform(){
    return this._platform;
  }
}

